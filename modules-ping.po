# Serbian translation of drupal (6.8)
# Copyright (c) 2009 by the Serbian translation team
# Generated from file: ping.module,v 1.52 2007/12/19 17:45:42 goba
#
msgid ""
msgstr ""
"Project-Id-Version: drupal (6.8)\n"
"POT-Creation-Date: 2009-02-22 10:38+0100\n"
"PO-Revision-Date: 2009-01-06 07:10+0100\n"
"Language-Team: Serbian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=((((n%10)==1)&&((n%100)!=11))?(0):(((((n%10)>=2)&&((n%10)<=4))&&(((n%100)<10)||((n%100)>=20)))?(1):2));\n"

#: modules/ping/ping.module:15
msgid ""
"The ping module is useful for notifying interested sites that your "
"site has changed. It automatically sends notifications, or \"pings\", "
"to the <a href=\"@external-http-pingomatic-com\">pingomatic</a> "
"service about new or updated content. In turn, <a "
"href=\"@external-http-pingomatic-com\">pingomatic</a> notifies other "
"popular services, including weblogs.com, Technorati, blo.gs, "
"BlogRolling, Feedster.com, and Moreover."
msgstr ""
"Пинг модул јо користан ако желите да "
"обавештавате одређене сајтове да се "
"ваш сајт променио. Он аутоматски шаље "
"обавештења, или \"пингује\", <a "
"href=\"@external-http-pingomatic-com\">pingomatic</a> сервис о "
"новом или промењеном садржају. "
"Заузврат, <a "
"href=\"@external-http-pingomatic-com\">pingomatic</a> "
"обавештава друге популарне сервисе, "
"укључујући  weblogs.com, Technorati, blo.gs, BlogRolling, "
"Feedster.com, и Moreover."

#: modules/ping/ping.module:16
msgid ""
"The ping module requires a correctly configured <a href=\"@cron\">cron "
"maintenance task</a>."
msgstr ""
"Пинг модул захтева исправно да је "
"исправно подешено <a href=\"@cron\">покретање "
"крона</a>."

#: modules/ping/ping.module:17
msgid ""
"For more information, see the online handbook entry for <a "
"href=\"@ping\">Ping module</a>."
msgstr ""
"За више информација, погледајте "
"приручник за <a href=\"@ping\">Пинг модул</a>."

#: modules/ping/ping.module:55
msgid "directory ping"
msgstr "пинг директоријума"

#: modules/ping/ping.module:55
msgid "Failed to notify pingomatic.com (site)."
msgstr ""
"Није успело обавештавање pingomatic.com "
"(сајта)."

#: modules/ping/ping.module:0
msgid "ping"
msgstr "пинг"

